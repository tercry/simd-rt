
// ===============================================================================================================
// -*- C++ -*-
//
// Raytracer.hpp - Declaration of the Raytracer renderer and the Ray class.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef RT_RAYTRACER_HPP
#define RT_RAYTRACER_HPP

#include "Common.hpp"

// If this is defined, the Raytracer will run in parallel on 4 threads.
// In the MT version, the screen is divided into 4 equal parts and each part
// of the final image is rendered by one thread, all 4 run in parallel.
#define SIMDRT_MULTITHREAD

namespace SimdRT
{

class Scene;
class Primitive;

///
/// Ray class.
/// Holds two vectors, one for the ray origin and one for the ray direction.
///
class Ray
{
public:

	Ray() : origin(0.0f, 0.0f, 0.0f), direction(0.0f, 0.0f, 0.0f) { }
	Ray(const Vector & orig, const Vector & dir) : origin(orig), direction(dir) { }

	Vector origin;
	Vector direction;
};

///
/// Intersection method return values.
///
enum HitType
{
	RAY_HIT  = 1, ///< Ray hit primitive
	RAY_MISS = 0, ///< Ray missed primitive
	RAY_IN_PRIMITIVE = -1 ///< Ray started inside primitive
};

///
/// Output pixel formats supported by the Raytracer.
///
enum PixelFormat
{
	PF_RGBA_UINT8,   ///< RGBA 8-bit [0,255] pixels
	PF_RGBA_FLOAT32  ///< RGBA 32-bit [0,1] normalized pixels
};

///
/// Raytracer engine class.
/// The raytracer is a singleton.
///
class Raytracer
{
private:

	 Raytracer() { /* Disabled */ }
	~Raytracer() { /* Disabled */ }

public:

	/// Initialize the RT engine. Must be called before rendering.
	static bool Initialize(int sceneWidth, int sceneHeight, PixelFormat pxFmt);

	/// Render the raytraced scene to a buffer.
	static void RenderScene(const Scene * theScene, const Vector & cameraPos, const Vector & screenPlane);

	/// Get a pointer to the output pixel buffer.
	static const void * GetOutputScene();

	/// Get the output pixel buffer dimensions.
	static void GetOutputDimensions(int & w, int & h);

	/// Get the output pixel buffer format.
	static PixelFormat GetOutputPixelFormat();

	/// Cleanup the mess.
	static void Finalize();

private:

#if defined (SIMDRT_MULTITHREAD)

	///
	/// Structure used to pass data to the threads.
	///
	struct RT_ThreadData
	{
		int x;
		int width;

		int y;
		int height;

		float sX;
		float sY;

		float dX;
		float dY;

		const Scene * sceneBuffer;
		Vector cameraPos;
	};

	/// Thread function to render a quarter of the image, based on the parameter.
	static void RendererThread(void * parameter);

#endif // SIMDRT_MULTITHREAD

	/// Trace a ray and accumulate the pixel color. Returns pointer to primitive hit by primary ray or null if no hit.
	static const Primitive * TraceRay(const Scene * theScene, const Ray & ray, Vector & color, float & dist);

	/// Write a pixel to the output buffer with current pixel format.
	static void PutPixel(const void * pixel, int x, int y);

private:

	// Pixel buffer data:
	static void * pixelBuf;
	static int width, height;
	static PixelFormat pixelFmt;
};

} // namespace SimdRT

#endif // RT_RAYTRACER_HPP
