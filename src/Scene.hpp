
// ===============================================================================================================
// -*- C++ -*-
//
// Scene.hpp - Scene related interfaces and clases.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef RT_SCENE_HPP
#define RT_SCENE_HPP

#include "Raytracer.hpp"
#include "INIFile.hpp"
#include <vector>

namespace SimdRT
{

///
/// Simple class to reresent object materials.
///
class Material
{
public:

	Material()
		: diffuse(0.2f)
		, reflection(0.0f)
		, specular(1.0f)
		, color(1.0f, 1.0f, 1.0f, 1.0f)
	{ }

	Material(float reflection, float diffuse, const Vector & color)
	{
		this->diffuse    = diffuse;
		this->reflection = reflection;
		this->specular   = (1.0f - this->diffuse);
		this->color      = color;
	}

	float diffuse;
	float reflection;
	float specular; // Specular = (1.0f - diffuse)
	Vector color;
};

///
/// Base class for geometric primitives.
///
class Primitive
{
public:

	Primitive()
		: name(0)
		, isLight(false)
		, material()
	{ }

	Primitive(const Material & mat, bool light, const char * name);

	const Material & GetMaterial() const   { return material; }
	void SetMaterial(const Material & mat) { material = mat;  }

	virtual HitType Intersect(const Ray & ray, float & dist) const = 0;
	virtual Vector GetNormal(const Vector & pos) const = 0;

	void SetAsLight(bool light) { isLight = light; }
	bool IsLight() const        { return isLight;  }

	void SetName(const char * name);
	const char * GetName() const { return name; }

	virtual ~Primitive();

protected:

	char * name;
	bool isLight;
	Material material;
};

///
/// Sphere primitive class definition.
///
class Sphere
	: public Primitive
{
public:

	Sphere(const Vector & c, float r, const Material & mat, bool light, const char * name)
		: Primitive(mat, light, name)
		, radius(r)
		, sqrRadius(r * r)
		, center(c)
	{ }

	/// Sphere -> ray intersection test.
	virtual HitType Intersect(const Ray & ray, float & dist) const;

	/// Get the normal vector at the given position.
	virtual Vector GetNormal(const Vector & pos) const;

public:

	float radius, sqrRadius;
	Vector center;
};

///
/// Plane primitive class definition.
///
class Plane
	: public Primitive
{
public:

	Plane(const Vector & normal, float d, const Material & mat, bool light, const char * name)
		: Primitive(mat, light, name)
		, data(normal.x, normal.y, normal.z, d)
	{ }

	/// Plane -> ray intersection test.
	virtual HitType Intersect(const Ray & ray, float & dist) const;

	/// Get the normal vector at the given position.
	virtual Vector GetNormal(const Vector & pos) const;

public:

	Vector data; ///< w is the D
};

///
/// Scene class definition.
///
class Scene
{
public:

	/// Build the scene.
	Scene(const INIFile * sceneCfg);

	/// Return the number of primitives in the scene.
	int GetNumPrimitives() const;

	/// Get a pointer to a scene object.
	const Primitive * GetPrimitive(int idx) const;

	~Scene();

private:

	std::vector<Primitive *> primitives;
};

} // namespace SimdRT

#endif // RT_SCENE_HPP
