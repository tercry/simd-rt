
// ===============================================================================================================
// -*- C++ -*-
//
// INIFile.hpp - Declaration of the INIFile class, for program configuration.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef RT_INI_FILE_HPP
#define RT_INI_FILE_HPP

#include "Common.hpp"
#include <string>
#include <list>
#include <map>

namespace SimdRT
{

///
/// INI file section.
///
class INISection
{
	friend class INIFile;

public:

	INISection(const std::string & sectionName);

	/// Get an integer value from the INI section, based on a key.
	bool GetInteger(const std::string & key, int & val) const;

	/// Get a float value from the INI section, based on a key.
	bool GetFloat(const std::string & key, float & val) const;

	/// Get a string from the INI section, based on a key.
	bool GetString(const std::string & key, std::string & val) const;

	/// Get a vector value from the INI section, based on a key.
	bool GetVector(const std::string & key, Vector & val) const;

	/// Get the section name (Read only).
	const std::string & GetName() const { return name; }

	/// Get the number of properties in this section.
	int GetNumProperties() const { return lines.size(); }

	~INISection();

private:

	typedef std::list<std::string> LineList;
	std::string name;
	LineList lines;
};

///
/// Standard .INI file parsing.
/// Supports sections and comments as ';' or '#'.
///
class INIFile
{
public:

	/*
	 * Interesting note, GCC on MacOS doesn't like the following declaration:
	 *
	 * typedef std::map<const std::string, const INISection *> SectionMap;
	 *
	 * Had to remove the 'const' qualifier from the string to compile.
	 * Did the same with the std::list in the class above.
	 */
	typedef std::map<std::string, INISection *> SectionMap;

	INIFile() { }

	/// Read an .INI file if 'fileName' is given.
	INIFile(const std::string & iniFile);

	/// Read an .INI file and split it into sections. If no sections are present, a "Global" section is defined.
	bool Read(const std::string & iniFile);

	/// Return pointer to a section, or null if the section does not exist.
	/// If 'strict' is false, the function tries to find the closest match to 'sectionName' if that exact section is not defined.
	const INISection * GetSection(const std::string & sectionName = "Global", bool strict = true) const;

	/// Return all loaded sections for the current .INI file.
	const SectionMap & GetAllSections() const { return sections; }

	/// Return the number of sections loaded.
	int GetNumSections() const { return sections.size(); }

	/// Delete all loaded sections and data.
	void Clear();

	/// Destructor does automatic cleanup.
	~INIFile();

private:

	SectionMap sections;
};

} // namespace SimdRT

#endif // RT_INI_FILE_HPP
