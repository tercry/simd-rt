
// ===============================================================================================================
// -*- C++ -*-
//
// Thread.cpp - Definition of a Thread wrapper class.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#include "Thread.hpp"
#include <assert.h>

#ifdef _MSC_VER

// Win32 Threads using process.h (MSDN recommended)

unsigned int __stdcall SimdRT::Thread::MyThreadProc(void * ctx)
{
	SimdRT::Thread * threadPtr = (SimdRT::Thread *)ctx;
	threadPtr->userFunc(threadPtr->userData);
	_endthreadex(0);
	return 0;
}

SimdRT::Thread::Thread(SimdRT::ThreadFunc fn, void * ud)
	: userFunc(fn)
	, userData(ud)
{
	unsigned int threadAddr;
	threadHandle = (void *)_beginthreadex(NULL, 0, MyThreadProc, this, 0, &threadAddr);
	assert(threadHandle != NULL);
}

void SimdRT::Thread::join()
{
	WaitForSingleObject(threadHandle, INFINITE);
}

SimdRT::Thread::~Thread()
{
	CloseHandle(threadHandle);
}

#else

// POSIX PThreads

void * SimdRT::Thread::MyThreadProc(void * ctx)
{
	SimdRT::Thread * threadPtr = (SimdRT::Thread *)ctx;
	threadPtr->userFunc(threadPtr->userData);
	pthread_exit(NULL);
}

SimdRT::Thread::Thread(SimdRT::ThreadFunc fn, void * ud)
	: userFunc(fn)
	, userData(ud)
{
	int result = pthread_create(&threadHandle, NULL, MyThreadProc, this);
	assert(result == 0);
}

void SimdRT::Thread::join()
{
	pthread_join(threadHandle, NULL);
}

SimdRT::Thread::~Thread()
{
}

#endif
